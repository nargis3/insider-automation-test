package tr.insider;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import tr.insider.driver.ThreadLocalChrome;
import tr.insider.pages.*;

import java.time.Duration;
import java.util.Set;


public class Main {
    public static void main(String[] args) {
        ThreadLocalChrome.chromeInitializeDriver();

        WebDriver driver = ThreadLocalChrome.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        driver.get("https://useinsider.com/");
        driver.manage().window().maximize();

        UseInsiderHomePage insiderHomePage = new UseInsiderHomePage(driver, wait);
        System.out.println(insiderHomePage.verifyHomeIsOpened());

        insiderHomePage.acceptCookies();
        insiderHomePage.openCompanyMenu();
        insiderHomePage.goToCareersPage();

        CareerPage careerPage = new CareerPage(driver, wait);
        System.out.println(careerPage.LocationsTitleIsDisplayed());
        System.out.println(careerPage.LifeAtInsiderTitleIsDisplayed());

        driver.get("https://useinsider.com/careers/quality-assurance/");
        QACareersPage qaCareersPage = new QACareersPage(driver, wait);
        System.out.println(qaCareersPage.verifyQaCareersIsOpened());
        qaCareersPage.clickAllQaJobs();

        driver.get("https://useinsider.com/careers/open-positions/?department=qualityassurance");
        QAOpenPositionsPage qaOpenPositionsPage = new QAOpenPositionsPage(driver, wait);
        System.out.println(qaOpenPositionsPage.verifyQaPositionsOpened());

        qaOpenPositionsPage.clickLocationFilter("Istanbul, Turkey");
        qaOpenPositionsPage.clickDepartmentFilter("Quality Assurance");

        String currentWindow = driver.getWindowHandle();

        qaOpenPositionsPage.waitForPositionsListThanNavigateToTheFirst();

        ViewRolePage viewRolePage = new ViewRolePage(driver, wait);

        Set<String> windows = driver.getWindowHandles();
        for (String window : windows)  {
            if (!window.equals(currentWindow)){
                driver.switchTo().window(window);
            }
        }
        System.out.println(viewRolePage.verifyViewRolePageTitle());
    }
}
