package tr.insider.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import tr.insider.pages.actions.ActionsHandler;

public class UseInsiderHomePage extends ActionsHandler {

    private final WebDriver driver;

    public UseInsiderHomePage (WebDriver driver, WebDriverWait wait){
        super(wait);
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//a[contains(text(),'Company')]")
    private WebElement companyMenu;

    @FindBy(xpath = "//a[contains(text(),'Careers')]")
    private WebElement careersBtn;

    @FindBy(xpath = "//a[text()='Accept All']")
    private WebElement acceptCookiesBtn;

    public boolean verifyHomeIsOpened(){
        return driver.getTitle().contains("#1 Leader in Individualized");
    }

    public void acceptCookies(){
        clickElement(acceptCookiesBtn);
    }

    public void openCompanyMenu(){
        companyMenu.click();
    }

    public void goToCareersPage(){
        clickElement(careersBtn);
    }

}
