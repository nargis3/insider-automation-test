package tr.insider.pages.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class ActionsHandler {
    private final WebDriverWait wait;

    public ActionsHandler(WebDriverWait wait) {
        this.wait = wait;
    }

    public void clickElement(WebElement element) {
        try {
            WebElement btn = wait.until(ExpectedConditions.elementToBeClickable(element));
            btn.click();
        } catch (Exception e) {
            System.out.println("Can't click because of: " + e.getMessage());
        }
    }

    public String getElementText(WebElement element) {
        wait.until(ExpectedConditions.visibilityOf(element));
        return element.getText();
    }

    public WebElement presenceOfElement(WebDriver driver, int maxSec, long intervalMS, String locator){
        FluentWait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(maxSec))
                .pollingEvery(Duration.ofMillis(intervalMS));
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(locator)));
    }

    public List<WebElement> elementsCount(WebDriver driver, int maxSec, long intervalMS, String locator, int elemCount) {
        FluentWait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(maxSec))
                .pollingEvery(Duration.ofMillis(intervalMS));
        return wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath(locator), elemCount));
    }
}
