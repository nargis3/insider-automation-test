package tr.insider.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import tr.insider.pages.actions.ActionsHandler;

public class ViewRolePage extends ActionsHandler {
    private final WebDriver driver;

    public ViewRolePage (WebDriver driver, WebDriverWait wait) {
        super(wait);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    public boolean verifyViewRolePageTitle(){
        return driver.getCurrentUrl().contains("lever");
    }
}
