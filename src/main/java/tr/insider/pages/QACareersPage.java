package tr.insider.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import tr.insider.pages.actions.ActionsHandler;

public class QACareersPage extends ActionsHandler {
    private final WebDriver driver;

    public QACareersPage (WebDriver driver, WebDriverWait wait) {
        super(wait);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//h1[contains(text(), 'Quality Assurance')]")
    private WebElement title;

    @FindBy(xpath = "//a[contains(text(), 'See all QA jobs')]")
    private WebElement allQaJobsBtn;

    public boolean verifyQaCareersIsOpened(){
        return driver.getTitle().contains("quality assurance");
    }

    public void clickAllQaJobs(){
        clickElement(allQaJobsBtn);
    }
}
