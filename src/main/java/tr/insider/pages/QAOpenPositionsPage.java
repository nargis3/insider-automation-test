package tr.insider.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import tr.insider.pages.actions.ActionsHandler;

import java.util.List;

public class QAOpenPositionsPage extends ActionsHandler {
    private final WebDriver driver;

    public QAOpenPositionsPage (WebDriver driver, WebDriverWait wait) {
        super(wait);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//h3[contains(text(),'All open positions')]")
    private WebElement pageTitle;

    @FindBy(xpath = "//span[@id='select2-filter-by-location-container']")
    private WebElement filterByLocationsBtn;

    @FindBy(xpath = "//span[@id='select2-filter-by-department-container']")
    private WebElement filterByDepartmentBtn;

    @FindBy(xpath = "//div[@id='jobs-list']//a[contains(text(), 'View Role')]")
    private WebElement viewRoleBtn;

    public boolean verifyQaPositionsOpened(){
        return driver.getTitle().contains("Insider") && getElementText(pageTitle).contains("All open positions");
    }

    public void clickLocationFilter(String location) {
        while (true){
            filterByLocationsBtn.click();
            try {
                presenceOfElement(driver, 10, 2000, "//li[contains(text(),'"+ location +"')]").click();
                break;
            } catch (Exception e){
                filterByLocationsBtn.click();
            }
        }
    }

    public void clickDepartmentFilter(String department) {
        while (true){
            filterByDepartmentBtn.click();
            try {
                JavascriptExecutor js = (JavascriptExecutor) driver;
                js.executeScript("window.scrollTo(0, document.body.scrollHeight);");
                presenceOfElement(driver, 10, 2000, "//li[contains(text(),'"+ department +"')]").click();
                break;
            } catch (Exception e){
                filterByDepartmentBtn.click();
            }
        }
    }

    public void waitForPositionsListThanNavigateToTheFirst(){
        while (true){
            List<WebElement> positions = elementsCount(driver, 10, 2000, "//div[@id='jobs-list']//span[contains(text(),'Quality Assurance')]", 0);
            if (!positions.isEmpty()){
                try {
                    goToPositionDetails(viewRoleBtn);
                    break;
                } catch (Exception e) {
                    throw new RuntimeException(e.getMessage());
                }
            }
        }
    }
    private void goToPositionDetails(WebElement element){
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", element);
    }
}
