package tr.insider.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import tr.insider.pages.actions.ActionsHandler;

public class CareerPage extends ActionsHandler {
    private final WebDriver driver;

    public CareerPage (WebDriver driver, WebDriverWait wait) {
        super(wait);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//h3[contains(text(),'Our Locations')]")
    private WebElement locationsTitleDisplayed;

    @FindBy(xpath = "//h2[contains(text(),'Life at Insider')]")
    private WebElement lifeAtInsiderTitleDisplayed;

    public boolean LocationsTitleIsDisplayed() {
        return getElementText(locationsTitleDisplayed).contains("Our Locations");
    }
    public boolean LifeAtInsiderTitleIsDisplayed() {
        return getElementText(lifeAtInsiderTitleDisplayed).contains("Life at Insider");
    }

}
