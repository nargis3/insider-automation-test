package tr.insider.driver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ThreadLocalChrome {

    public static ThreadLocal<ChromeDriver> driverThreadLocal = new ThreadLocal<ChromeDriver>();

    public static synchronized void chromeInitializeDriver(){

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setBinary("/Users/nargizmirzazada/IdeaProjects/seleniumLearningNew/src/main/resources/chrome-mac-arm64/Google Chrome for Testing.app/Contents/MacOS/Google Chrome for Testing");
        System.setProperty("webdriver.chrome.driver", "/Users/nargizmirzazada/IdeaProjects/seleniumLearningNew/src/main/resources/chromedriver-mac-arm64/chromedriver");
        driverThreadLocal.set(new ChromeDriver(chromeOptions));
    }

    public static synchronized ChromeDriver getDriver(){
        return driverThreadLocal.get();
    }
}
